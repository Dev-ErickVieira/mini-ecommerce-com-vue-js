import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Produto from "./views/Produto.vue"
import Login from "./views/Login.vue"
import Usuario from "./views/usuario/Usuario.vue"
import UsuarioProdutos from "./views/usuario/UsuarioProdutos.vue"
import UsuarioEditar from "./views/usuario/UsuarioEditar.vue"
import UsuarioCompras from "./views/usuario/UsuarioCompras.vue"
import UsuarioVendas from "./views/usuario/UsuarioVendas.vue"
import NotFoundComponent from "./components/NotFoundComponent.vue"

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "*",
      component: NotFoundComponent,
    },
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: '/produto/:id',
      name: "produto",
      component: Produto,
      props: true
    },
    {
      path: '/login',
      name: "login",
      component: Login,
    },
    {
      path: '/usuario',
      component: Usuario,
      meta: {
        login: true
      },
      children: [
        {
          path: "",
          name: "Usuario",
          component: UsuarioProdutos,
        },
        {
          path: "/compras",
          name: "UsuarioCompras",
          component: UsuarioCompras,
        },
        {
          path: "/editar",
          name: "UsuarioEditar",
          component: UsuarioEditar,
        },
        {
          path: "/vendas",
          name: "UsuarioVendas",
          component: UsuarioVendas,
        },
      ]
    }
  ],
  scrollBehavior() {
    return window.scrollTo({top: 0, behavior: "smooth"})
  }
});


router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.login)) {
    if (!window.localStorage.token) {
      next("/login")
    } else {
      next()
    }
  } else {
    next()
  }
}) 

export default router;