/* Login */
export const UPDATE_LOGIN = "updateLogin"

/* Usuario */
export const UPDATE_USUARIO = 'updateUsuario'
export const UPDATE_USUARIO_PRODUTOS = 'updateUsuarioProdutos'
export const ADD_USUARIO_PRODUTOS = 'addUsuarioProdutos'