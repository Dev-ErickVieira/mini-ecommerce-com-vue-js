import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import { API_URL } from "@/common/config.js";

Vue.use(VueAxios, axios);

const api = Vue.axios.create({
  baseURL: API_URL
});

api.interceptors.request.use(
  function (config) {
    const token = window.localStorage.token
    if (token) {
      config.headers.Authorization = token
    } 
    return config
  },
  function (error) {
    return Promise.reject(erro)
  }
)

export const ApiService = {
  get(endpoint) {
    return api.get(endpoint).catch(error => {
      throw new Error(` [RANEK] ApiService ${error}`);
    });
  },
  post(endpoint, body) {
    return api.post(endpoint, body);
  },
  delete(endpoint) {
    return api.delete(endpoint);
  },
  put(endpoint, body) {
    return api.put(endpoint, body);
  },
  login(body) {
    return axios.post("http://localhost/ranek/wp-json/jwt-auth/v1/token", body);
  },
  validateToken() {
    return api.post("http://localhost/ranek/wp-json/jwt-auth/v1/token/validate");
  }
};

export function getCep(cep) {
  return Vue.axios.get(`http://viacep.com.br/ws/${cep}/json/`)
}
